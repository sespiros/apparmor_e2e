# Adding a New Test

Tests are defined in [`src/e2e/mod.rs`](../src/e2e/mod.rs). Because of the way
[Rust modules][rust-modules] are structured, tests must be defined in this file
to implement the `Test` trait (though they can import code in other files).

Tests are currently limited to a single process running under an AppArmor
profile. This is expected to change to test features that require multiple
processes such as signal restrictions.

To add a new test, define a new `struct` and implement the top level `Test`
trait. The trait defines a path for the test to run as, setup and teardown
methods for the framework to use, and a test function to run as a seperate
process:

[rust-modules]: https://doc.rust-lang.org/1.30.0/book/second-edition/ch07-00-modules.html

```rust
struct TestNew {}

impl Test for TestNew {
    // The test name also doubles as a path for this test to be written to.
    fn name(&self) -> &str {
        return "/usr/sbin/apparmor_e2e_new";
    }

    fn setup(&self) -> io::Result<()> {
        return load_profile(
            // File name to write the profile to.
            "new_profile",
            // Important that the profile targets the value name() returns.
            "
profile new_profile /usr/sbin/apparmor_e2e_new {
    #include<abstractions/test>

    # specify profile for the test to run as.
    /tmp/ r,
}
",
        );
    }

    fn teardown(&self) -> io::Result<()> {
        // Needs to be the same file name as passed to load_profile.
        return remove_profile("new_profile");
    }

    // This method will run in a seperate process. apparmor_e2e re-execs this
    // test under the path returned by name().
    fn run(&self) {
        // Use assert_ok, assert_err, and assert_eq! in tests with a descrption
        // of what operation failed.
        assert_ok(fs::read_dir("/"), "reading /");
        assert_ok(fs::read_dir("/tmp"), "reading /tmp");
    }
}
```

Register the new test by adding it to the vector the top level tests() method
returns:

```rust
pub fn tests<'a> -> Vec<&'a Test> {
    return vec![&TestFiles {}, &TestRegexPriority {}, &TestNew {}];
}
```
