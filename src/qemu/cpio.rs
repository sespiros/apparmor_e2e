use std::ffi;
use std::fs;
use std::io;
use std::io::prelude::*;
use std::mem;
use std::path;

extern crate libc;

fn err_short_write<T>() -> io::Result<T> {
    return Err(io::Error::new(
        io::ErrorKind::Other,
        "wrote fewer bytes than last header specified",
    ));
}

fn err_long_write<T>() -> io::Result<T> {
    return Err(io::Error::new(
        io::ErrorKind::Other,
        "wrote more bytes than last header specified",
    ));
}

#[derive(Debug)]
pub struct Header {
    pub name: String,
    pub size: u32,

    inode: u32,
    mode: u32,
    uid: u32,
    gid: u32,
    nlink: u32,
    mtime: u32,
    dev_major: u32,
    dev_minor: u32,
    rdev_major: u32,
    rdev_minor: u32,
}

fn write_hex<'a, W: io::Write>(w: &'a mut W, n: u32) -> io::Result<()> {
    write!(w, "{:08X}", n)?;
    return Ok(());
}

static MAGIC: &'static [u8] = &[0x30, 0x37, 0x30, 0x37, 0x30, 0x31]; // 070701
static TRAILER: &'static str = "TRAILER!!!";

impl Header {
    /// Determine header information from a path's file info.
    ///
    /// Returned names are canonicalized relative to "/".
    pub fn from_path<P: AsRef<path::Path>>(path: P) -> io::Result<Header> {
        let path_str = path.as_ref().to_string_lossy().to_string();
        let p = ffi::CString::new(path_str.clone())?;
        let mut stat: libc::stat = unsafe { mem::zeroed() };
        let rc = unsafe { libc::lstat(p.as_ptr(), &mut stat) };
        if rc < 0 {
            return Err(io::Error::last_os_error());
        }

        let mut h = Header {
            inode: stat.st_ino as u32,
            mode: stat.st_mode,
            uid: stat.st_uid,
            gid: stat.st_gid,
            nlink: stat.st_nlink as u32,
            mtime: stat.st_mtime as u32,
            size: stat.st_size as u32,
            // Minor is the lowest 2 bytes, major is everything else.
            dev_major: (stat.st_dev >> 8) as u32,
            dev_minor: (stat.st_dev & 0xff) as u32,
            rdev_major: (stat.st_rdev >> 8) as u32,
            rdev_minor: (stat.st_rdev & 0xff) as u32,
            // Canonicalize path.
            name: path_str.trim_start_matches("/").to_string(),
        };

        if h.is_symlink() {
            // Symlinks size is the size of the file they point to
            h.size = fs::read_link(path)?.to_string_lossy().len() as u32;
        } else if !h.is_file() {
            // Set the size for anything that's not a regular file to 0
            h.size = 0
        }

        return Ok(h);
    }

    pub fn symlink<P: AsRef<path::Path>>(name: P, target: P) -> Header {
        return Header {
            name: name.as_ref().to_string_lossy().to_string(),
            mode: libc::S_IFLNK | 0o777,

            uid: 0,
            gid: 0,
            inode: 0,
            nlink: 0,
            mtime: 0,
            size: target.as_ref().to_string_lossy().len() as u32,
            dev_major: 0,
            dev_minor: 0,
            rdev_major: 0,
            rdev_minor: 0,
        };
    }

    pub fn directory<P: AsRef<path::Path>>(path: P, mode: u32) -> Header {
        return Header {
            name: path.as_ref().to_string_lossy().to_string(),
            mode: libc::S_IFDIR | (mode & 0o777),

            uid: 0,
            gid: 0,
            inode: 0,
            nlink: 2,
            mtime: 0,
            size: 0,
            dev_major: 0,
            dev_minor: 0,
            rdev_major: 0,
            rdev_minor: 0,
        };
    }

    pub fn new(path: String) -> Header {
        return Header {
            inode: 0,
            mode: 0,
            uid: 0,
            gid: 0,
            nlink: 0,
            mtime: 0,
            size: 0,
            dev_major: 0,
            dev_minor: 0,
            rdev_major: 0,
            rdev_minor: 0,
            name: path,
        };
    }
}

impl Header {
    pub fn is_symlink(&self) -> bool {
        return self.mode & libc::S_IFMT == libc::S_IFLNK;
    }

    pub fn is_file(&self) -> bool {
        return self.mode & libc::S_IFMT == libc::S_IFREG;
    }
}

/// Used to track the number of bytes written to the underlying writer to
/// determine padding.
struct WriteCounter<W: io::Write> {
    /// Number of bytes written to the writer.
    n: usize,

    w: W,
}

impl<W: io::Write> io::Write for WriteCounter<W> {
    fn write(&mut self, b: &[u8]) -> io::Result<usize> {
        self.n += b.len();
        return self.w.write(b);
    }

    fn flush(&mut self) -> io::Result<()> {
        return self.w.flush();
    }
}

/// A CPIO archive constructor. The format is intended for operability with the
/// initrd format (SVR4 with no CRC). It is not ment for general use.
pub struct Builder<W: io::Write> {
    n: usize,
    w: WriteCounter<io::BufWriter<W>>,
}

impl<W: io::Write> Builder<W> {
    pub fn new(w: W) -> Builder<W> {
        return Builder {
            n: 0,
            w: WriteCounter {
                n: 0,
                w: io::BufWriter::new(w),
            },
        };
    }

    fn pad(&mut self) -> io::Result<()> {
        // (header|path name) and file data must be padded to a multiple of 4.
        let to_pad = self.w.n % 4;
        if to_pad == 0 {
            return Ok(());
        }
        let pad: [u8; 3] = [0; 3];
        self.w.write(&pad[to_pad - 1..])?;
        return Ok(());
    }

    /// Write a CPIO header for the next file. The actual contents of the file
    /// should then be written using write().
    ///
    /// Files must be written serially.
    pub fn write_header<'a>(&mut self, h: &'a Header) -> io::Result<()> {
        if self.n > 0 {
            return err_short_write();
        }
        self.pad()?;

        self.w.write(MAGIC)?;
        let w = &mut self.w;
        write_hex(w, h.inode)?;
        write_hex(w, h.mode)?;

        write_hex(w, h.uid)?;
        write_hex(w, h.gid)?;

        write_hex(w, h.nlink)?;
        write_hex(w, h.mtime)?;

        write_hex(w, h.size)?;

        write_hex(w, h.dev_major)?;
        write_hex(w, h.dev_minor)?;
        write_hex(w, h.rdev_major)?;
        write_hex(w, h.rdev_minor)?;

        write_hex(w, (h.name.len() + 1) as u32)?;
        write_hex(w, 0)?; // write check, always 0

        write!(w, "{}", h.name)?;
        w.write(&[0])?;
        self.pad()?;
        self.n = h.size as usize;
        return Ok(());
    }

    /// Write the archive trailer and flush any pending data to the underlying
    /// writter.
    ///
    /// Unlike flush, this should only be called once to finalize the archive.
    pub fn write_trailer(&mut self) -> io::Result<()> {
        if self.n > 0 {
            return err_short_write();
        }
        self.pad()?;
        let mut h = Header::new(TRAILER.to_string());
        h.nlink = 1;
        self.write_header(&h)?;
        return self.w.flush();
    }
}

impl<W: io::Write> io::Write for Builder<W> {
    fn write(&mut self, b: &[u8]) -> io::Result<usize> {
        if self.n < b.len() {
            return err_long_write();
        }
        self.n -= b.len();
        return self.w.write(b);
    }

    fn flush(&mut self) -> io::Result<()> {
        return self.w.flush();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::string;
    use std::u32;
    use std::vec;

    #[test]
    fn test_header_from_path_file() {
        let h = Header::from_path("test/file.txt").unwrap();
        assert_eq!(h.size, 4);
        assert_eq!(h.nlink, 1);
        assert!(h.is_file());
        assert!(!h.is_symlink());
    }

    #[test]
    fn test_header_from_path_symlink() {
        let h = Header::from_path("test/file.txt.link").unwrap();
        assert_eq!(h.size, 8);
        assert_eq!(h.nlink, 1);
        assert!(!h.is_file());
        assert!(h.is_symlink());
    }

    #[test]
    fn test_header_directory() {
        let h = Header::directory("proc".to_string(), 0o755);
        assert_eq!(h.mode, 0o40755);
    }

    fn assert_eq_str(v: Vec<u8>, s: &str) {
        assert_eq!(string::String::from_utf8(v).unwrap(), s);
    }

    #[test]
    fn test_write_hex() {
        let mut v = vec::Vec::new();
        write_hex(&mut v, 9).unwrap();
        assert_eq_str(v, "00000009");

        let mut v = vec::Vec::new();
        write_hex(&mut v, u32::MAX).unwrap();
        assert_eq_str(v, "FFFFFFFF");

        let mut v = vec::Vec::new();
        write_hex(&mut v, 1234).unwrap();
        write_hex(&mut v, 4321).unwrap();
        assert_eq_str(v, "000004D2000010E1");
    }

    #[test]
    fn test_write_archive() {
        let test_data = include_bytes!("../../test/a.cpio").to_vec();

        let mut v = vec::Vec::new();
        {
            let mut b = Builder::new(&mut v);

            let h = Header {
                inode: 7221000,
                mode: 33184,
                uid: 583664,
                gid: 89939,
                nlink: 1,
                mtime: 1553118483, // "2019-03-20T14:48:03-07:00
                size: 4,
                dev_major: 254,
                dev_minor: 1,
                rdev_major: 0,
                rdev_minor: 0,
                name: "file.txt".to_string(),
            };
            b.write_header(&h).unwrap();
            write!(b, "hi!\n").unwrap();
            b.write_trailer().unwrap();
        }

        assert_eq!(
            string::String::from_utf8(test_data[..v.len()].to_vec()).unwrap(),
            string::String::from_utf8(v).unwrap()
        );
    }
}
